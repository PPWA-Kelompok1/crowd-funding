# Crowd Funding

Nama-nama Kelompok 1:
Ardian Ghifari (1706039452)
Razaqa Dhafin Haffiyan (1706039484)
Gusti Fahmi Fadhila (1706044036)
Adzkia Aisyah Afrah Hardian (1706075035)

# Pipelines:
[![pipeline status](https://gitlab.com/PPWA-Kelompok1/crowd-funding/badges/master/pipeline.svg)](https://gitlab.com/PPWA-Kelompok1/crowd-funding/commits/master)

# Code Coverage:
[![coverage report](https://gitlab.com/PPWA-Kelompok1/crowd-funding/badges/master/coverage.svg)](https://gitlab.com/PPWA-Kelompok1/crowd-funding/commits/master)

Link herokuapp: http://cscare.herokuapp.com/
