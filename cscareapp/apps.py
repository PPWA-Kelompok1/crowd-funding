from django.apps import AppConfig


class CscareappConfig(AppConfig):
    name = 'cscareapp'
