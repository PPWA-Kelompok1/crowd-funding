from django.conf.urls import url
from django.urls import path
from .views import *
from django.contrib.auth import views as auth_views

# app_name = "cscareapp"
urlpatterns = [
	path('', view_homepage, name='homepage'),
	path('register/', register, name = 'register'),
	path('add_donatur/', add_donatur, name='add_donatur'),
	path('tentang/', tentang, name='tentang'),
	path('testimoni/', testimoni, name='testimoni'),
	path('add_testimoni/', add_testimoni, name='add_testimoni'),
	path('program/<nama_program>/', program, name='program'),
	path('donasi/<nama_program>/', donasi, name = 'donasi'),
	path('add_donasi/<nama_program>/', add_donasi, name='add_donasi'),
	path('logout/', logout, name = 'logout'),
	path('donations/', donations, name = 'donations'),
]
