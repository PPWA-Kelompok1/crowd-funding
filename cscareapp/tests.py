from django.test import TestCase, Client
from django.urls import resolve
from django.db import IntegrityError
from .views import *
from .models import *
from .forms import *
from django.contrib.auth.models import User

class TPTest(TestCase):

    def test_cscareapp_url_home_page_exist(self):
        nama = "Duka Palu, Duka Kita Bersama"
        deskripsi = "Kami melalui CS Care mengajak sahabat sekalian untuk turut serta membantu saudara-saudara kita di Sulawesi Tengah agar mampu bangkit keluar dari tragedi yang memilukan hati ini. Donasi sahabat sekalian bisa menjadi pelipur lara bagi mereka."
        foto = "disasters/palu.jpg"
        new_disaster = Disasters.objects.create(nama = nama, deskripsi = deskripsi, foto = foto)
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_cscareapp_url_register_page_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_cscareapp_url_about_page_exist(self):
        response = Client().get('/tentang/')
        self.assertEqual(response.status_code, 200)

    def test_cscareapp_url_testimoni_page_exist(self):
        response = Client().get('/testimoni/')
        self.assertEqual(response.status_code, 200)

    def test_cscareapp_url_donations_page_exist(self):
        response = Client().get('/donations/')
        self.assertEqual(response.status_code, 200)

    def test_cscareapp_url_logout_page_exist(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)

    # def test_cscareapp_url_program_page_exist(self):
    #     nama = "test"
    #     deskripsi = "test"
    #     foto = "test"
    #     new_disaster = Disasters.objects.create(nama = nama, deskripsi = deskripsi, foto = foto)
    #
    #     response = Client().get('/program/test/')
    #     self.assertEqual(response.status_code, 200)

    def test_cscareapp_url_donasi_page_exist(self):
        nama = "test"
        deskripsi = "test"
        foto = "test"
        new_disaster = Disasters.objects.create(nama = nama, deskripsi = deskripsi, foto = foto)

        response = Client().get('/donasi/test/')
        self.assertEqual(response.status_code, 200)

    def test_cscareapp_url_page_not_exist(self):
        response = Client().get('/dummy_page')
        self.assertEqual(response.status_code, 404)

    def test_cscareapp_using_home_template(self):
        nama = "Duka Palu, Duka Kita Bersama"
        deskripsi = "Kami melalui CS Care mengajak sahabat sekalian untuk turut serta membantu saudara-saudara kita di Sulawesi Tengah agar mampu bangkit keluar dari tragedi yang memilukan hati ini. Donasi sahabat sekalian bisa menjadi pelipur lara bagi mereka."
        foto = "disasters/palu.jpg"
        new_disaster = Disasters.objects.create(nama = nama, deskripsi = deskripsi, foto = foto)
        response = Client().get('')
        self.assertTemplateUsed(response, 'Home Page.html')

    def test_cscareapp_using_register_template(self):
        response = Client().get('/register/')
        self.assertTemplateUsed(response, 'Register Page.html')

    def test_cscareapp_using_about_template(self):
        response = Client().get('/tentang/')
        self.assertTemplateUsed(response, 'About Page.html')

    # def test_cscareapp_using_program_template(self):
    #     nama = "test"
    #     deskripsi = "test"
    #     foto = "test"
    #     new_disaster = Disasters.objects.create(nama = nama, deskripsi = deskripsi, foto = foto)
    #
    #     response = Client().get('/program/test/')
    #     self.assertTemplateUsed(response, 'Program Page.html')

    def test_cscareapp_using_donasi_template(self):
        nama = "test"
        deskripsi = "test"
        foto = "test"
        new_disaster = Disasters.objects.create(nama = nama, deskripsi = deskripsi, foto = foto)

        response = Client().get('/donasi/test/')
        self.assertTemplateUsed(response, 'Donasi Page.html')

    def test_cscareapp_view_homepage_views(self):
        found = resolve('/')
        self.assertEqual(found.func, view_homepage)

    def test_cscareapp_register_views(self):
        found = resolve('/register/')
        self.assertEqual(found.func, register)

    def test_cscareapp_about_views(self):
        found = resolve('/tentang/')
        self.assertEqual(found.func, tentang)

    def test_cscareapp_donations_views(self):
        found = resolve('/donations/')
        self.assertEqual(found.func, donations)

    def test_cscareapp_add_donations_views(self):
        found = resolve('/add_donasi/test/')
        self.assertEqual(found.func, add_donasi)

    def test_cscareapp_add_donatur_views(self):
        found = resolve('/add_donatur/')
        self.assertEqual(found.func, add_donatur)

    def test_cscareapp_program_views(self):
        nama = "test"
        deskripsi = "test"
        foto = "test"
        new_disaster = Disasters.objects.create(nama = nama, deskripsi = deskripsi, foto = foto)

        found = resolve('/program/test/')
        self.assertEqual(found.func, program)

    def test_cscareapp_donasi_views(self):
        nama = "test"
        deskripsi = "test"
        foto = "test"
        new_disaster = Disasters.objects.create(nama = nama, deskripsi = deskripsi, foto = foto)

        found = resolve('/donasi/test/')
        self.assertEqual(found.func, donasi)

    def test_model_can_make_new_account(self):
        nama = "test"
        birth_date = "1999-01-01"
        email = "test@gmail.com"
        password = "test"
        new_account = Accounts.objects.create(name = nama, birth_date = birth_date, email = email, password = password)
        counting_all_available_accounts = Accounts.objects.all().count()
        self.assertEqual(counting_all_available_accounts, 1)

    def test_model_reject_exist_email(self):
        nama1 = "test1"
        birth_date1 = "1999-01-01"
        email1 = "test@gmail.com"
        password1 = "test1"
        new_account = Accounts.objects.create(name = nama1, birth_date = birth_date1, email = email1, password = password1)
        nama2 = "test2"
        birth_date2 = "2000-01-01"
        email2 = "test@gmail.com"
        password2 = "test2"
        with self.assertRaises(IntegrityError):
            other_account = Accounts.objects.create(name = nama2, birth_date = birth_date2, email = email2, password = password2)

    def test_model_reject_birthdate_over_year(self):
        name = "test"
        birth_date = "3000-01-01"
        email = "test@gmail.com"
        password = "test"
        new_account = Accounts.objects.create(name = name, birth_date = birth_date, email = email, password = password)
        counting_all_available_accounts = Accounts.objects.all().count()
        self.assertEqual(counting_all_available_accounts, 0)

    def test_model_reject_birthdate_over_day(self):
        name = "test"
        birth_date = "2018-12-31"
        email = "test@gmail.com"
        password = "test"
        new_account = Accounts.objects.create(name = name, birth_date = birth_date, email = email, password = password)
        counting_all_available_accounts = Accounts.objects.all().count()
        self.assertEqual(counting_all_available_accounts, 0)

    def test_model_can_make_new_donation(self):
        program = "test"
        donatur = "test"
        email = "test@gmail.com"
        uang = 5000000
        visibilitas = True
        new_donation = Donations.objects.create(program = program, donatur = donatur, email = email, uang = uang, visibilitas = visibilitas)
        counting_all_available_donations = Donations.objects.all().count()
        self.assertEqual(counting_all_available_donations, 1)

    def test_model_can_make_new_disaster(self):
        nama = "test"
        deskripsi = "test"
        foto = "test"
        new_disaster = Disasters.objects.create(nama = nama, deskripsi = deskripsi, foto = foto)
        counting_all_available_disasters = Disasters.objects.all().count()
        self.assertEqual(counting_all_available_disasters, 1)

    def test_post_register_success(self):
        name = "test"
        birth_date = "1999-01-01"
        email = "test@gmail.com"
        password = "test"
        response_post = Client().post('/add_donatur', {'name':name, 'birth_date':birth_date, 'email':email, 'password':password})
        self.assertEqual(response_post.status_code, 301)

    def test_post_donasi_success(self):
        program = "test"
        donatur = "test"
        email = "test@gmail.com"
        uang = 5000000
        visibilitas = True
        response_post = Client().post('/add_donasi/test', {'program':program, 'donatur':donatur, 'email':email, 'uang':uang, 'visibilitas':visibilitas})
        self.assertEqual(response_post.status_code, 301)

    def test_post_count_testimoni_success(self):
        nama = "dummy"
        komentar = "dummy"
        response_post = Client().post('/add_testimoni/', {'nama':nama, 'komentar':komentar})
        self.assertEqual(response_post.status_code, 200)
        counting_all_available_testimoni = Testimoni.objects.all().count()
        self.assertEqual(counting_all_available_testimoni, 1)

    # def test_post_count_donasi_visible_success(self):
    #     session = self.client.session
    #     session['user_id'] = 11
    #     session['name'] = 'test'
    #     session['email'] = 'email@test.com'
    #     session.save()
    #     # user = User.objects.create(username='testuser', email='email@gmail.com')
    #     # user.set_password('hello')
    #     # user.save()
    #     # user = authenticate(username='testuser', password='hello')
    #     # Client().login(username='testuser', password='hello')
    #     program = "test"
    #     donatur = "test"
    #     email = "test@gmail.com"
    #     uang = 5000000
    #     visibilitas = True
    #     response_post = Client().post('/add_donasi/test/', {'uang':uang, 'visibilitas':visibilitas})
    #     self.assertEqual(response_post.status_code, 302)
    #     counting_all_available_donations = Donations.objects.all().count()
    #     self.assertEqual(counting_all_available_donations, 1)
    #
    # def test_post_count_donasi_not_visible_success(self):
    #     session = self.client.session
    #     session['user_id'] = 11
    #     session['name'] = 'test'
    #     session['email'] = 'email@test.com'
    #     session.save()
    #     # user = User.objects.create(username='testuser', email='email@gmail.com')
    #     # user.set_password('hello')
    #     # user.save()
    #     # user = authenticate(username='testuser', password='hello')
    #     # Client().login(username='testuser', password='hello')
    #     program = "test"
    #     donatur = "test"
    #     email = "test@gmail.com"
    #     uang = 5000000
    #     response_post = Client().post('/add_donasi/test/', {'uang':uang})
    #     self.assertEqual(response_post.status_code, 302)
    #     counting_all_available_donations = Donations.objects.all().count()
    #     self.assertEqual(counting_all_available_donations, 1)

    def test_post_count_donatur_success(self):
        name = "test"
        birth_date = "1999-01-01"
        email = "ok@gmail.com"
        password = "test123456"
        response_post = Client().post('/add_donatur/', {'name':name, 'birth_date':birth_date, 'email':email, 'password':password})
        self.assertEqual(response_post.status_code, 302)
        counting_all_available_accounts = Accounts.objects.all().count()
        self.assertEqual(counting_all_available_accounts, 1)
