from django import forms

class Register_Form(forms.Form):
    name = forms.CharField(label = "Nama Lengkap", required = True, max_length = 100, widget=forms.TextInput(attrs= {'class' : 'form-control'}))
    birth_date = forms.DateField(label = "Tanggal Lahir", required = True, widget = forms.DateInput(attrs = {'type': 'date','class' : 'form-control'}))
    email = forms.EmailField(label = "Email", required = True, widget = forms.EmailInput(attrs = {'class' : 'form-control'}))
    password = forms.CharField(label = "Password", required = True, min_length = 8, widget = forms.PasswordInput(attrs = {'class' : 'form-control'}))

class Donation_Form(forms.Form):
    # program = forms.CharField(label = "Nama Program", required = True, widget = forms.TextInput(attrs= {'class' : 'form-control'}))
    # donatur = forms.CharField(label = "Nama Lengkap", required = True, max_length = 100, widget=forms.TextInput(attrs= {'class' : 'form-control'}))
    # email = forms.EmailField(label = "Email", required = True, widget = forms.EmailInput(attrs = {'class' : 'form-control'}))
    uang = forms.IntegerField(label = "Jumlah Rp", required = True,  widget=forms.TextInput(attrs= {'type': 'number','class' : 'form-control'}))
    visibilitas = forms.BooleanField(label = "Menampilkan Nama", required = False, widget = forms.CheckboxInput(attrs= {'class' : 'form-control'}))

class Testimoni_Form(forms.Form):
    komentar = forms.CharField(
        label = "Komentar anda mengenai kami?",
        required = True,
        widget = forms.Textarea(
        attrs = {
        'placeholder' : 'Bagaimana kesanmu berbagi dengan CSCare?',
        'class' : 'form-control',
        'data-length' : 300}))
