from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from .forms import *
from .models import *
from django.contrib.auth import logout as auth_logout

# Create your views here.
def get_response():
	lokasi = 'Sinar Perak Tower, Setiabudi, Jakarta South'
	nomor_hp = '085811916028'
	email = 'cscare@sinarperak.com'
	tentang_kami = 'Kami adalah komunitas mahasiswa yang ingin membantu saudara setanah air yang sedang dilanda musibah.'
	response = {'lokasi': lokasi, 'nomor_hp': nomor_hp, 'email': email, 'tentang_kami': tentang_kami}
	return response

def view_homepage(request):
	response = get_response()
	disaster = Disasters.objects.get(nama='Duka Palu, Duka Kita Bersama')
	response['current_program'] = disaster.nama
	response['dana_terkumpul'] = 99999999
	response['total_donatur'] = 10000
	response['disasters'] = Disasters.objects.exclude(nama=disaster.nama)
	response['range'] = range(2, 8)
	return render(request, 'Home Page.html', response)

def register(request):
	response = get_response()
	form = Register_Form()
	response['form'] = form
	return render(request, 'Register Page.html', response)

def program(request, nama_program):
	if request.user.is_authenticated:
		response = get_response()
		disaster = Disasters.objects.get(nama=nama_program)
		disasters = Disasters.objects.exclude(nama=nama_program)[0:3]
		response['nama_program'] = disaster.nama
		response['foto_program'] = disaster.foto
		response['deskripsi_program'] = disaster.deskripsi
		response['disasters'] = disasters
		response['range'] = range(2, 5)
		return render(request, 'Program Page.html', response)
	else:
		return HttpResponseRedirect('/register')

def add_donatur(request):
	form = Register_Form(request.POST or None)
	response = {}
	if(request.method == 'POST' and form.is_valid()):
		if(Accounts.objects.filter(email=request.POST['email']).count() == 0):
			response['name'] = request.POST['name']
			response['birth_date'] = request.POST['birth_date']
			response['email'] = request.POST['email']
			response['password'] = request.POST['password']
			new_donatur = Accounts(name = response['name'],
	            birth_date = response['birth_date'],
	            email = response['email'],
	            password = response['password'])
			new_donatur.save()
	return HttpResponseRedirect('/register')

def donasi(request, nama_program):
	response = get_response()
	disaster = Disasters.objects.get(nama=nama_program)
	disasters = Disasters.objects.exclude(nama=nama_program)[0:3]
	response['nama_program'] = disaster.nama
	response['form'] = Donation_Form()
	return render(request, 'Donasi Page.html', response)

def add_donasi(request, nama_program):
	form = Donation_Form(request.POST or None)
	response = {}
	if(request.method == 'POST' and form.is_valid()):
		response['program'] = nama_program
		response['donatur'] = request.user.id
		response['email'] = request.user.email
		response['uang'] = request.POST['uang']
		if('visibilitas' in request.POST):
			response['visibilitas'] = True
			new_donasi = Donations(program = response['program'],
			donatur = response['donatur'],
			email = response['email'],
			uang = response['uang'],
			visibilitas = response['visibilitas'])
		else:
			response['visibilitas'] = False
			new_donasi = Donations(program = response['program'],
			donatur = response['donatur'],
			email = response['email'],
			uang = response['uang'],
			visibilitas = response['visibilitas'])
		new_donasi.save()
	return HttpResponseRedirect('/donations')

def donations(request):
	response = get_response()
	daftar_donasi = Donations.objects.filter(donatur=str(request.user.id))
	response['daftar_donasi'] = daftar_donasi
	return render(request, 'Donations Page.html', response)

def logout(request):
    auth_logout(request)
    return HttpResponseRedirect('/')

def tentang(request):
	response = get_response()
	response['form'] = Testimoni_Form()
	response['testimoni'] = Testimoni.objects.all()
	return render(request, 'About Page.html', response)

def testimoni(request):
	testi = list(Testimoni.objects.values())
	data = {
		'testi': testi
    }
	return JsonResponse(data)

def add_testimoni(request):
	if request.method == 'POST':
		nama = request.POST['nama']
		komentar = request.POST['komentar']
		testi = Testimoni(nama=nama, komentar=komentar)
		testi.save()
	return JsonResponse(testi.as_dict())
