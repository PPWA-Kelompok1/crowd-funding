$(document).ready(function() {
    //carousel options
    getAllData();
    $('#quote-carousel').carousel({
        pause: true, interval: 5000,
    });
});

var addTesti = function(nama, komentar) {
    $.ajax({
        type: "POST",
        url: "/add_testimoni/",
        data: {
            nama: nama,
            komentar: komentar,
            csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
        },
        success: function() {
            $('#id_komentar').val('');
        },
        error: function (error) {
            alert('tidak berhasil disimpan');
        }
    });
};

$("#form").on("submit", function(event) {
    event.preventDefault();
    var nama = $("#id_nama").val();
    var komentar = $("#id_komentar").val();
    addTesti(nama, komentar);
    getAllData();
    $('#quote-carousel').carousel({
        pause: true, interval: 5000,
    });
});

var getAllData = function() {
    $('#testimoni_slider').empty();
    $('#slide_num').empty();
    $.ajax({
        type: 'GET',
        url: '/testimoni',
        data: {
            get_param: 'value'
        },
        dataType: 'json',
        success: function (data) {
            var isian = '';
            var slider = '';
            for (var i = 0; i < data.testi.length; i++) {
                if (i == 0) {
                    isian += '<div class="carousel-item active"><div class="row"><div class="col-sm-12"><p>' +
                    data.testi[0].komentar + '</p><small><strong>' +
                    data.testi[0].nama + '</strong></small></div></div></div>';
                    slider += '<li data-target="#quote-carousel" data-slide-to=' + 0 + ' class="active"></li>';
                } else {
                    isian += '<div class="carousel-item"><div class="row"><div class="col-sm-12"><p>' +
                    data.testi[i].komentar + '</p><small><strong>' +
                    data.testi[i].nama + '</strong></small></div></div></div>';
                    slider += '<li data-target="#quote-carousel" data-slide-to=' + i + '></li>';
                }
            }
            $('#slide_num').append(slider);
            $('#testimoni_slider').append(isian);
        }
    });
}
