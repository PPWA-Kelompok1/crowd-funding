from datetime import date
from django.db import models

# Create your models here.
class Accounts(models.Model):
    name = models.TextField(max_length=100)
    birth_date = models.DateField()
    email = models.EmailField(unique = True)
    password = models.TextField(max_length=20)

    def save(self, *args, **kwargs):
        tahun = int(self.birth_date.split('-')[0])
        bulan = int(self.birth_date.split('-')[1])
        tanggal = int(self.birth_date.split('-')[2])
        now = date.today()
        if (now.year > tahun):
            super(Accounts, self).save(*args, **kwargs)
        elif (now.year == tahun and now.month > bulan):
            super(Accounts, self).save(*args, **kwargs)
        elif (now.year == tahun and now.month == bulan and now.day > tanggal):
            super(Accounts, self).save(*args, **kwargs)


class Donations(models.Model):
    program = models.TextField(max_length = 100)
    donatur = models.TextField(max_length = 100)
    email = models.EmailField()
    uang = models.IntegerField()
    visibilitas = models.BooleanField(default = False)

class Disasters(models.Model):
    nama = models.TextField()
    deskripsi = models.TextField()
    foto = models.TextField()

class Testimoni(models.Model):
    nama = models.TextField()
    komentar = models.TextField()

    def as_dict(self):
        return {
            "nama": self.nama,
            "komentar": self.komentar
        }
